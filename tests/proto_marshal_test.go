package tests

import (
	"github.com/golang/protobuf/proto"
	gogo "gitlab.com/go-yp/go-warning-codegeneration/models/protos/gogo/advertisement"
	google "gitlab.com/go-yp/go-warning-codegeneration/models/protos/google/advertisement"
	"testing"
)

const (
	n = 1000
)

func TestGoogleProtoMarshal(t *testing.T) {
	for i := 0; i < n; i++ {
		var popup = &google.Popup{
			Id:      uint32(i),
			Viewed:  true,
			Clicked: false,
		}

		go func() {
			proto.Marshal(popup)
		}()

		go func() {
			popup.Clicked = true

			proto.Marshal(popup)
		}()
	}
}

func TestGogoProtoMarshal(t *testing.T) {
	for i := 0; i < n; i++ {
		var popup = &gogo.Popup{
			Id:      uint32(i),
			Viewed:  true,
			Clicked: false,
		}

		go func() {
			proto.Marshal(popup)
		}()

		go func() {
			popup.Clicked = true

			proto.Marshal(popup)
		}()
	}
}
