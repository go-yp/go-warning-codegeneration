package tests

import (
	"encoding/json"
	"github.com/stretchr/testify/require"
	"gitlab.com/go-yp/go-warning-codegeneration/models/jsons/easy"
	"gitlab.com/go-yp/go-warning-codegeneration/models/jsons/standard"
	"testing"
)

const (
	// language=JSON
	popupWithUnicodeContent = `{
        "title": "Some title with symbol \u201Dt",
        "description": "Any description"
    }`

	// language=JSON
	popupContent = `{
        "title": "Some title",
        "description": "Any description"
    }`
)

func TestStandardUnmarshalJSON(t *testing.T) {
	content := make([]byte, 0, 1024)

	content = append(content[:0], popupWithUnicodeContent...)

	var popup standard.Popup

	unmarshalErr := json.Unmarshal(content, &popup)

	require.NoError(t, unmarshalErr)

	var expected = standard.Popup{
		Title:       "Some title with symbol \u201Dt",
		Description: "Any description",
	}

	require.Equal(t, expected, popup)

	content = append(content[:0], popupContent...)

	require.Equal(t, expected, popup)
}

func TestEasyjsonUnmarshalJSON(t *testing.T) {
	content := make([]byte, 0, 1024)

	content = append(content[:0], popupWithUnicodeContent...)

	var popup easy.Popup

	unmarshalErr := json.Unmarshal(content, &popup)

	require.NoError(t, unmarshalErr)

	var expected = easy.Popup{
		Title:       "Some title with symbol \u201Dt",
		Description: "Any description",
	}

	require.Equal(t, expected, popup)

	content = append(content[:0], popupContent...)

	/**
	Failed:
	expected: easy.Popup{Title:"Some title with symbol ”t", Description:"Any description"}
	actual  : easy.Popup{Title:"Some title with symbol ”t", Description:" }y description"}
	*/
	require.Equal(t, expected, popup)
}
