module gitlab.com/go-yp/go-warning-codegeneration

go 1.14

require (
	github.com/gogo/protobuf v1.3.1
	github.com/golang/protobuf v1.4.2
	github.com/mailru/easyjson v0.7.6
	github.com/stretchr/testify v1.6.1
)
