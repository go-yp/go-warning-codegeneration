proto-install:
	PROTOC_ZIP=protoc-3.11.4-linux-x86_64.zip
	curl -OL https://github.com/protocolbuffers/protobuf/releases/download/v3.11.4/$PROTOC_ZIP
	sudo unzip -o $PROTOC_ZIP -d /usr/local bin/protoc
	sudo unzip -o $PROTOC_ZIP -d /usr/local 'include/*'
	rm -f $PROTOC_ZIP

gogofaster-install:
	go get github.com/gogo/protobuf/protoc-gen-gogofaster

easyjson-install:
	go get -u github.com/mailru/easyjson/...

# install tools on clear system
install: proto-install gogofaster-install easyjson-install

proto-generate:
	rm -rf ./models/protos
	mkdir -p ./models/protos
	protoc -I. --go_out=${GOPATH}/src protos/google/advertisement/*.proto
	protoc -I. --gogofaster_out=${GOPATH}/src protos/gogo/advertisement/*.proto

easyjson-generate:
	easyjson ./models/jsons/easy

test:
	go test ./tests/... -v -count=1