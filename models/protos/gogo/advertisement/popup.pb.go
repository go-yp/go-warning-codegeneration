// Code generated by protoc-gen-gogo. DO NOT EDIT.
// source: protos/gogo/advertisement/popup.proto

package advertisement

import (
	fmt "fmt"
	proto "github.com/gogo/protobuf/proto"
	io "io"
	math "math"
	math_bits "math/bits"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.GoGoProtoPackageIsVersion3 // please upgrade the proto package

type Popup struct {
	Id      uint32 `protobuf:"varint,1,opt,name=id,proto3" json:"id,omitempty"`
	Viewed  bool   `protobuf:"varint,2,opt,name=viewed,proto3" json:"viewed,omitempty"`
	Clicked bool   `protobuf:"varint,3,opt,name=clicked,proto3" json:"clicked,omitempty"`
}

func (m *Popup) Reset()         { *m = Popup{} }
func (m *Popup) String() string { return proto.CompactTextString(m) }
func (*Popup) ProtoMessage()    {}
func (*Popup) Descriptor() ([]byte, []int) {
	return fileDescriptor_6d7842379769fe24, []int{0}
}
func (m *Popup) XXX_Unmarshal(b []byte) error {
	return m.Unmarshal(b)
}
func (m *Popup) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	if deterministic {
		return xxx_messageInfo_Popup.Marshal(b, m, deterministic)
	} else {
		b = b[:cap(b)]
		n, err := m.MarshalToSizedBuffer(b)
		if err != nil {
			return nil, err
		}
		return b[:n], nil
	}
}
func (m *Popup) XXX_Merge(src proto.Message) {
	xxx_messageInfo_Popup.Merge(m, src)
}
func (m *Popup) XXX_Size() int {
	return m.Size()
}
func (m *Popup) XXX_DiscardUnknown() {
	xxx_messageInfo_Popup.DiscardUnknown(m)
}

var xxx_messageInfo_Popup proto.InternalMessageInfo

func (m *Popup) GetId() uint32 {
	if m != nil {
		return m.Id
	}
	return 0
}

func (m *Popup) GetViewed() bool {
	if m != nil {
		return m.Viewed
	}
	return false
}

func (m *Popup) GetClicked() bool {
	if m != nil {
		return m.Clicked
	}
	return false
}

func init() {
	proto.RegisterType((*Popup)(nil), "advertisement.Popup")
}

func init() {
	proto.RegisterFile("protos/gogo/advertisement/popup.proto", fileDescriptor_6d7842379769fe24)
}

var fileDescriptor_6d7842379769fe24 = []byte{
	// 207 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0xe2, 0x52, 0x2d, 0x28, 0xca, 0x2f,
	0xc9, 0x2f, 0xd6, 0x4f, 0xcf, 0x4f, 0xcf, 0xd7, 0x4f, 0x4c, 0x29, 0x4b, 0x2d, 0x2a, 0xc9, 0x2c,
	0x4e, 0xcd, 0x4d, 0xcd, 0x2b, 0xd1, 0x2f, 0xc8, 0x2f, 0x28, 0x2d, 0xd0, 0x03, 0xcb, 0x0b, 0xf1,
	0xa2, 0x48, 0x29, 0x79, 0x72, 0xb1, 0x06, 0x80, 0x64, 0x85, 0xf8, 0xb8, 0x98, 0x32, 0x53, 0x24,
	0x18, 0x15, 0x18, 0x35, 0x78, 0x83, 0x98, 0x32, 0x53, 0x84, 0xc4, 0xb8, 0xd8, 0xca, 0x32, 0x53,
	0xcb, 0x53, 0x53, 0x24, 0x98, 0x14, 0x18, 0x35, 0x38, 0x82, 0xa0, 0x3c, 0x21, 0x09, 0x2e, 0xf6,
	0xe4, 0x9c, 0xcc, 0xe4, 0xec, 0xd4, 0x14, 0x09, 0x66, 0xb0, 0x04, 0x8c, 0xeb, 0x94, 0x7a, 0xe2,
	0x91, 0x1c, 0xe3, 0x85, 0x47, 0x72, 0x8c, 0x0f, 0x1e, 0xc9, 0x31, 0x4e, 0x78, 0x2c, 0xc7, 0x70,
	0xe1, 0xb1, 0x1c, 0xc3, 0x8d, 0xc7, 0x72, 0x0c, 0x51, 0xde, 0xe9, 0x99, 0x25, 0x39, 0x89, 0x49,
	0x7a, 0xc9, 0xf9, 0xb9, 0xfa, 0xe9, 0xf9, 0xba, 0x95, 0x05, 0x20, 0xb2, 0x3c, 0xb1, 0x28, 0x2f,
	0x33, 0x2f, 0x5d, 0x37, 0x39, 0x3f, 0x25, 0x35, 0x3d, 0x35, 0x2f, 0xb5, 0x28, 0xb1, 0x24, 0x33,
	0x3f, 0x4f, 0x3f, 0x37, 0x3f, 0x25, 0x35, 0xa7, 0x58, 0x1f, 0xa7, 0x67, 0x92, 0xd8, 0xc0, 0x52,
	0xc6, 0x80, 0x00, 0x00, 0x00, 0xff, 0xff, 0xf6, 0xe2, 0x30, 0x4f, 0xf0, 0x00, 0x00, 0x00,
}

func (m *Popup) Marshal() (dAtA []byte, err error) {
	size := m.Size()
	dAtA = make([]byte, size)
	n, err := m.MarshalToSizedBuffer(dAtA[:size])
	if err != nil {
		return nil, err
	}
	return dAtA[:n], nil
}

func (m *Popup) MarshalTo(dAtA []byte) (int, error) {
	size := m.Size()
	return m.MarshalToSizedBuffer(dAtA[:size])
}

func (m *Popup) MarshalToSizedBuffer(dAtA []byte) (int, error) {
	i := len(dAtA)
	_ = i
	var l int
	_ = l
	if m.Clicked {
		i--
		if m.Clicked {
			dAtA[i] = 1
		} else {
			dAtA[i] = 0
		}
		i--
		dAtA[i] = 0x18
	}
	if m.Viewed {
		i--
		if m.Viewed {
			dAtA[i] = 1
		} else {
			dAtA[i] = 0
		}
		i--
		dAtA[i] = 0x10
	}
	if m.Id != 0 {
		i = encodeVarintPopup(dAtA, i, uint64(m.Id))
		i--
		dAtA[i] = 0x8
	}
	return len(dAtA) - i, nil
}

func encodeVarintPopup(dAtA []byte, offset int, v uint64) int {
	offset -= sovPopup(v)
	base := offset
	for v >= 1<<7 {
		dAtA[offset] = uint8(v&0x7f | 0x80)
		v >>= 7
		offset++
	}
	dAtA[offset] = uint8(v)
	return base
}
func (m *Popup) Size() (n int) {
	if m == nil {
		return 0
	}
	var l int
	_ = l
	if m.Id != 0 {
		n += 1 + sovPopup(uint64(m.Id))
	}
	if m.Viewed {
		n += 2
	}
	if m.Clicked {
		n += 2
	}
	return n
}

func sovPopup(x uint64) (n int) {
	return (math_bits.Len64(x|1) + 6) / 7
}
func sozPopup(x uint64) (n int) {
	return sovPopup(uint64((x << 1) ^ uint64((int64(x) >> 63))))
}
func (m *Popup) Unmarshal(dAtA []byte) error {
	l := len(dAtA)
	iNdEx := 0
	for iNdEx < l {
		preIndex := iNdEx
		var wire uint64
		for shift := uint(0); ; shift += 7 {
			if shift >= 64 {
				return ErrIntOverflowPopup
			}
			if iNdEx >= l {
				return io.ErrUnexpectedEOF
			}
			b := dAtA[iNdEx]
			iNdEx++
			wire |= uint64(b&0x7F) << shift
			if b < 0x80 {
				break
			}
		}
		fieldNum := int32(wire >> 3)
		wireType := int(wire & 0x7)
		if wireType == 4 {
			return fmt.Errorf("proto: Popup: wiretype end group for non-group")
		}
		if fieldNum <= 0 {
			return fmt.Errorf("proto: Popup: illegal tag %d (wire type %d)", fieldNum, wire)
		}
		switch fieldNum {
		case 1:
			if wireType != 0 {
				return fmt.Errorf("proto: wrong wireType = %d for field Id", wireType)
			}
			m.Id = 0
			for shift := uint(0); ; shift += 7 {
				if shift >= 64 {
					return ErrIntOverflowPopup
				}
				if iNdEx >= l {
					return io.ErrUnexpectedEOF
				}
				b := dAtA[iNdEx]
				iNdEx++
				m.Id |= uint32(b&0x7F) << shift
				if b < 0x80 {
					break
				}
			}
		case 2:
			if wireType != 0 {
				return fmt.Errorf("proto: wrong wireType = %d for field Viewed", wireType)
			}
			var v int
			for shift := uint(0); ; shift += 7 {
				if shift >= 64 {
					return ErrIntOverflowPopup
				}
				if iNdEx >= l {
					return io.ErrUnexpectedEOF
				}
				b := dAtA[iNdEx]
				iNdEx++
				v |= int(b&0x7F) << shift
				if b < 0x80 {
					break
				}
			}
			m.Viewed = bool(v != 0)
		case 3:
			if wireType != 0 {
				return fmt.Errorf("proto: wrong wireType = %d for field Clicked", wireType)
			}
			var v int
			for shift := uint(0); ; shift += 7 {
				if shift >= 64 {
					return ErrIntOverflowPopup
				}
				if iNdEx >= l {
					return io.ErrUnexpectedEOF
				}
				b := dAtA[iNdEx]
				iNdEx++
				v |= int(b&0x7F) << shift
				if b < 0x80 {
					break
				}
			}
			m.Clicked = bool(v != 0)
		default:
			iNdEx = preIndex
			skippy, err := skipPopup(dAtA[iNdEx:])
			if err != nil {
				return err
			}
			if skippy < 0 {
				return ErrInvalidLengthPopup
			}
			if (iNdEx + skippy) < 0 {
				return ErrInvalidLengthPopup
			}
			if (iNdEx + skippy) > l {
				return io.ErrUnexpectedEOF
			}
			iNdEx += skippy
		}
	}

	if iNdEx > l {
		return io.ErrUnexpectedEOF
	}
	return nil
}
func skipPopup(dAtA []byte) (n int, err error) {
	l := len(dAtA)
	iNdEx := 0
	depth := 0
	for iNdEx < l {
		var wire uint64
		for shift := uint(0); ; shift += 7 {
			if shift >= 64 {
				return 0, ErrIntOverflowPopup
			}
			if iNdEx >= l {
				return 0, io.ErrUnexpectedEOF
			}
			b := dAtA[iNdEx]
			iNdEx++
			wire |= (uint64(b) & 0x7F) << shift
			if b < 0x80 {
				break
			}
		}
		wireType := int(wire & 0x7)
		switch wireType {
		case 0:
			for shift := uint(0); ; shift += 7 {
				if shift >= 64 {
					return 0, ErrIntOverflowPopup
				}
				if iNdEx >= l {
					return 0, io.ErrUnexpectedEOF
				}
				iNdEx++
				if dAtA[iNdEx-1] < 0x80 {
					break
				}
			}
		case 1:
			iNdEx += 8
		case 2:
			var length int
			for shift := uint(0); ; shift += 7 {
				if shift >= 64 {
					return 0, ErrIntOverflowPopup
				}
				if iNdEx >= l {
					return 0, io.ErrUnexpectedEOF
				}
				b := dAtA[iNdEx]
				iNdEx++
				length |= (int(b) & 0x7F) << shift
				if b < 0x80 {
					break
				}
			}
			if length < 0 {
				return 0, ErrInvalidLengthPopup
			}
			iNdEx += length
		case 3:
			depth++
		case 4:
			if depth == 0 {
				return 0, ErrUnexpectedEndOfGroupPopup
			}
			depth--
		case 5:
			iNdEx += 4
		default:
			return 0, fmt.Errorf("proto: illegal wireType %d", wireType)
		}
		if iNdEx < 0 {
			return 0, ErrInvalidLengthPopup
		}
		if depth == 0 {
			return iNdEx, nil
		}
	}
	return 0, io.ErrUnexpectedEOF
}

var (
	ErrInvalidLengthPopup        = fmt.Errorf("proto: negative length found during unmarshaling")
	ErrIntOverflowPopup          = fmt.Errorf("proto: integer overflow")
	ErrUnexpectedEndOfGroupPopup = fmt.Errorf("proto: unexpected end of group")
)
