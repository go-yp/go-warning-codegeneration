package standard

type Popup struct {
	Title       string `json:"title"`
	Description string `json:"description"`
}
