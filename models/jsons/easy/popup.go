package easy

// easyjson:json
type Popup struct {
	Title       string `json:"title"`
	Description string `json:"description"`
}
